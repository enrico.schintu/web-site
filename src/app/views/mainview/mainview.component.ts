import {Component, OnInit} from '@angular/core';
import {TopNavLink} from './../../shared/components/topnavbar/TopNavLink';
import {Router} from '@angular/router';

@Component({
  selector: 'app-mainview',
  templateUrl: './mainview.component.html',
  styleUrls: ['./mainview.component.css']
})
export class MainviewComponent implements OnInit {

  public linksTopNavBar: TopNavLink[];
  public indexLinkAttivo: number;

  constructor(private router: Router) {
    this.getTopNavbarLink();
  }

  ngOnInit() {
    this.topLinkClicked(0);
  }

  /** Viene cliccato un link dalla top navbar */
  public topLinkClicked(index: number) {
    this.indexLinkAttivo = index;
    this.router.navigate([this.linksTopNavBar[index].target]);
  }

  // Genera i link da mostrare nella topNavBar
  private getTopNavbarLink(): void {
    this.linksTopNavBar = [];
    this.linksTopNavBar.push(new TopNavLink('Home', 'home', false));
    this.linksTopNavBar.push(new TopNavLink('Chi sono', 'aboutme', false));
    this.linksTopNavBar.push(new TopNavLink('Formazione', 'training', false));
    this.linksTopNavBar.push(new TopNavLink('Esperienza', 'exp', false));
    this.linksTopNavBar.push(new TopNavLink('Abilità', 'skills', false));
    this.linksTopNavBar.push(new TopNavLink('Contatti', 'contacts', false));
  }

}
