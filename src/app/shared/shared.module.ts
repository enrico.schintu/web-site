import { ViewMenuComponent } from './components/view-menu/view-menu.component';
import { PanelComponent } from './components/panel/panel.component';
import { TimelineComponent } from './components/timeline/timeline.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ImgContainerComponent } from './components/img-container/img-container.component';
import { HomeComponent } from './components/home/home.component';
import { ContactsComponent } from './components/contacts/contacts.component';
import { AboutmeComponent } from './components/aboutme/aboutme.component';
import { FooterComponent } from './components/footer/footer.component';
import { TopnavbarComponent } from './components/topnavbar/topnavbar.component';
import { ExpComponent } from './components/exp/exp.component';
import { SkillsComponent } from './components/skills/skills.component';
import { TrainingComponent } from './components/training/training.component';
import {ProgressiveBarComponent} from './components/progressive-bar/progressive-bar.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    BrowserAnimationsModule,
  ],
  declarations: [
    TopnavbarComponent,
    FooterComponent,
    ImgContainerComponent,
    HomeComponent,
    AboutmeComponent,
    ExpComponent,
    ContactsComponent,
    SkillsComponent,
    TrainingComponent,
    TimelineComponent,
    PanelComponent,
    ProgressiveBarComponent,
    ViewMenuComponent
  ],
  exports: [
    TopnavbarComponent,
    FooterComponent,
    HomeComponent,
    AboutmeComponent,
    ExpComponent,
    ContactsComponent,
    SkillsComponent,
    TrainingComponent,
    TimelineComponent,
    PanelComponent,
    ProgressiveBarComponent
  ]
})
export class SharedModule { }
