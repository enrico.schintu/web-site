import { TimePoint } from './../timeline/TimePoint';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-view-menu',
  templateUrl: './view-menu.component.html',
  styleUrls: ['./view-menu.component.scss']
})
export class ViewMenuComponent implements OnInit {

  @Input() points: TimePoint[];
  @Input() activePoint: number;
  @Output() pointSelected: EventEmitter<any> = new EventEmitter<any>();

  constructor() { 
    this.points = [];
  }

  ngOnInit() {
  }

  public selectPoint(selectedPoint: number): void {
    this.pointSelected.emit(selectedPoint);
  }

}
