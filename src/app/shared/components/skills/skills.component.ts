import {Component, OnInit} from '@angular/core';
import {TimePoint} from '../timeline/TimePoint';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {

  public timePoints: TimePoint[];
  public selectedPointIndex: number;
  public selectedPoint: TimePoint;

  constructor() {
    this.generateTimePoints();
    this.selectTimePoint(0);
  }

  ngOnInit() {
  }

  public selectTimePoint(selectedPointIndex: number): void {
    this.selectedPointIndex = selectedPointIndex;
    this.selectedPoint = this.timePoints[selectedPointIndex];
  }

  private generateTimePoints(): void {
    this.timePoints = [];
    this.timePoints.push(new TimePoint('Conoscenza Lingue', 'Conoscenze Linguistiche', null, null));
    this.timePoints.push(new TimePoint('Work Skills', 'Abilità lavorative', null, null));
    this.timePoints.push(new TimePoint('Front End', 'Front End Skills', null, null));
    this.timePoints.push(new TimePoint('Back End', 'Back End Skills', null, null));
  }

}
