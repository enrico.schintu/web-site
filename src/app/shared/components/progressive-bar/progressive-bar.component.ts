import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-progressive-bar',
  templateUrl: './progressive-bar.component.html',
  styleUrls: ['./progressive-bar.component.scss']
})
export class ProgressiveBarComponent implements OnInit {

  @Input() label: string;
  @Input() percent: string;
  @Input() level: string;

  public inPagePercent: string;

  constructor() {
    this.label = '';
    this.inPagePercent = '0%';
    this.level = '';
  }

  ngOnInit() {
    setTimeout( () => {
      this.inPagePercent = this.percent;
    }, 50);
  }

}
