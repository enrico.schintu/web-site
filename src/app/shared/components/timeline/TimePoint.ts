export class TimePoint {
    label: string;
    value: string;
    luogo: string;
    descrizione: any;

    constructor(label: string, value: string, luogo: string, descrizione: any) {
        this.label = label;
        this.value = value;
        this.luogo = luogo;
        this.descrizione = descrizione;
    }
}
