import { TimePoint } from './TimePoint';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {

  @Input() points: TimePoint[];
  @Input() activePoint: number;
  @Output() pointSelected: EventEmitter<any> = new EventEmitter<any>();

  constructor() { 
    this.points = [];
  }

  ngOnInit() {
  }

  public selectPoint(selectedPoint: number): void {
    this.pointSelected.emit(selectedPoint);
  }

}
