import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss']
})
export class PanelComponent implements OnInit {

  @Input() title: string;
  @Input() data: string;
  @Input() luogo: string;
  @Input() descrizione: string;

  public collapsed: boolean;

  constructor() {
    this.title = '';
    this.data = '';
    this.luogo = '';
    this.descrizione = '';
  }

  ngOnInit() {
  }

}
