import { TimePoint } from './../timeline/TimePoint';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.scss']
})
export class TrainingComponent implements OnInit {

  public timePoints: TimePoint[];
  public selectedPointIndex: number;
  public selectedPoint: TimePoint;

  constructor() {
    this.generateTimePoints();
    this.selectTimePoint(0);
  }

  ngOnInit() {
    window.scrollTo(0, 0);
  }

  public selectTimePoint(selectedPointIndex: number): void {
    this.selectedPointIndex = selectedPointIndex;
    this.selectedPoint = this.timePoints[selectedPointIndex];
  }

  private generateTimePoints(): void {
    this.timePoints = [];
    const activity1 = ['- Programmazione utilizzando Java Script, PHP, HTML, CSS, Visual Basic', '- Inglese, Francese', '- Diritto', '- Economia, Finanza, Ragioneria'];
    const activity2 = [`- Apprendimento dell'uso di sistemi operativi basati su UNIX e dei loro relativi software`, `- Programmazione in C, Java, JavaScript, PHP, HTML, Assembler, SQL`, `- Creazione e gestione di applicazioni client/server`];
    this.timePoints.push(new TimePoint('2006', 'Diploma di Ragioniere Perito Programmatore', `Istituto Tecnico e Tecnologico “S. Satta”, Macomer (Italia)`, activity1));
    this.timePoints.push(new TimePoint('2015', 'Laurea in Ingegneria Informatica', `Politecnico di Torino, Torino (Italia)`, activity2));
  }
}
