import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TopNavLink } from './TopNavLink';
import {Router} from '@angular/router';

@Component({
  selector: 'app-topnavbar',
  templateUrl: './topnavbar.component.html',
  styleUrls: ['./topnavbar.component.scss']
})
export class TopnavbarComponent implements OnInit {

  @Input() title: string;
  @Input() subTitle: string;
  @Input() routerLinks: TopNavLink[];
  @Input() indexActiveLink: number;
  @Output() linkClicked: EventEmitter<number> = new EventEmitter<number>();

  public activeSmallMenu: boolean;

  constructor(private router: Router) {
    this.title = '';
    this.subTitle = '';
    this.routerLinks = [];
    this.indexActiveLink = 0;
  }

  ngOnInit() {
  }

  onLinkClick(index: number): void {
    this.linkClicked.emit(index);
    this.activeSmallMenu = false;
  }

}
