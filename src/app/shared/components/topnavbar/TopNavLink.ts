export class TopNavLink {
    label: string;
    target: string;
    active: boolean;

    constructor (label: string, target: string, active: boolean) {
        this.label = label;
        this.target = target;
        this.active = active;
    }
}
