import {Component, OnInit} from '@angular/core';
import {TimePoint} from '../timeline/TimePoint';

@Component({
  selector: 'app-exp',
  templateUrl: './exp.component.html',
  styleUrls: ['./exp.component.scss']
})
export class ExpComponent implements OnInit {

  public timePoints: TimePoint[];
  public selectedPointIndex: number;
  public selectedPoint: TimePoint;

  constructor() {
    this.populateTimePoints();
    this.selectTimePoint(0);

  }

  ngOnInit() {
    window.scrollTo(0, 0);

  }

  public selectTimePoint(selectedPointIndex: number): void {
    this.selectedPointIndex = selectedPointIndex;
    this.selectedPoint = this.timePoints[selectedPointIndex];
  }

  private populateTimePoints(): void {
    this.timePoints = [];
    const adv_location = 'AddValue S.P.A. (Verona)';
    const home_location = 'Norbello (Oristano)';
    const advExp1 = [`Sviluppo di applicazioni Web/Batch in contesto bancario e commerciale attraverso l'utilizzo della tecnologia Java
    combinata con tecnologie per sviluppo web. Framework utilizzati: Spring e JQuery.`];
    const advExp2 = [`Analisi e sviluppo di applicazioni Web/Batch in contesto bancario e commerciale attraverso l'utilizzo della tecnologia Java
    combinata con tecnologie per sviluppo web. Framework utilizzati: Spring, Spring Batch, Jquery, Bootstrap, Angular 5, Magento.`];
    const advExp3 = [`Analisi e sviluppo di applicazioni Web/Batch in contesto bancario e commerciale attraverso l'utilizzo della tecnologia Java
    combinata con tecnologie per sviluppo web. Framework utilizzati: Spring, Spring Batch, Jquery, Bootstrap, Angular 5.`];
    const homeExp1 = [`Sviluppo di videogiochi per Android utilizzando Unity3D Engine combinato con i linguaggi Javascripts e C#`,
      `Sviluppo di applicazioni Web utilizzando il MEAN stack e LAMP stack`];
    this.timePoints.push(new TimePoint('Giu 2010', 'Sviluppatore Indipendente', home_location, homeExp1));
    this.timePoints.push(new TimePoint('Ago 2015', 'Stage AddValue', adv_location, advExp1));
    this.timePoints.push(new TimePoint('Ott 2015', 'Apprendistato AddValue', adv_location, advExp2));
    this.timePoints.push(new TimePoint('Apr 2018', 'Sviluppatore AddValue', adv_location, advExp3));
  }

}
