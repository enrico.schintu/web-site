import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-img-container',
  templateUrl: './img-container.component.html',
  styleUrls: ['./img-container.component.scss']
})
export class ImgContainerComponent implements OnInit {

  @Input() src: string;
  @Input() height: string;
  @Input() title: string;

  public hover: boolean;

  constructor() {
   }

  ngOnInit() {
    if (!this.height) {
      this.height = '120px';
    }
  }

}
