import { Routes } from '@angular/router';

import { ContactsComponent } from './shared/components/contacts/contacts.component';
import { SkillsComponent } from './shared/components/skills/skills.component';
import { ExpComponent } from './shared/components/exp/exp.component';
import { AboutmeComponent } from './shared/components/aboutme/aboutme.component';
import { TrainingComponent } from './shared/components/training/training.component';
import { HomeComponent } from './shared/components/home/home.component';

export const appRoutes: Routes = [
    { path: 'home', component:  HomeComponent},
    { path: 'aboutme', component:  AboutmeComponent},
    { path: 'training', component:  TrainingComponent},
    { path: 'exp', component:  ExpComponent},
    { path: 'skills', component:  SkillsComponent},
    { path: 'contacts', component:  ContactsComponent},
    { path: '**', redirectTo: '/home'}
];
